## Poker hand
This app deals you five cards and determines the best combination according to poker rules.

### Features
* By default the app uses one deck, deals five random cards, determines the best combination
    and displays the original hand, the name of the combination and the cards involved in it.
* Number of decks can be set through a form.
* There is a _cheat_ feature which enables submission of a custom hand of cards and uses that
    for evaluation.



Screenshot:

![screenshot](http://i.imgur.com/Kxn6Em3.png?1)

### Setup
* Clone the repo
* `cd pokerhand`
* Run `composer install` to grab dependencies.

### Use with built-in PHP server
* `$ php -S localhost:8000 -t web/`
* Navigate your browser to http://localhost:8000/app.php/pokerhand

### Use with Docker
* Make sure your port `80` is not in use.
* `$ docker-compose up -d`
* Add `127.0.0.1 pokerhand.dev` to your `hosts` file
* Navigate your browser to http://pokerhand.dev/pokerhand

### Components
The main features are contained in `PokerHandBundle`.
* `Dealer`: handles decks and dealing. Uses a `MixerInterface` implementation.
    Number of decks to be used can be set by parameter of its `mix` method.
* `BasicMixer`: implements `MixerInterface`.
* `DeckFactory`: responsible to generate card decks.
* `HandEvaluator`: this contains the main business logic to find best combination of a given hand.
* `ParsedHand`: used by `HandEvaluator` to parse and collect data of the hand and return cards.

### Testing
Unit test are included in the `tests/PokerhandBundle/` directory. Run the test with:
```
    $ ./vendor/bin/phpunit tests/PokerhandBundle
```