<?php

namespace PokerhandBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class PokerhandController extends Controller
{
    public function getHandAction(Request $request)
    {
        /** @var Form $cheatForm */
        $cheatForm = $this->createFormBuilder()
            ->add('cards', TextType::class, ['label' => 'Cards: '])
            ->add('send', SubmitType::class, ['label' => 'Cheat!'])
            ->getForm();

        $cheatForm->handleRequest($request);

        /** @var Form $deckForm */
        $deckForm = $this->createFormBuilder()
            ->add('decks', IntegerType::class, ['label' => 'Number of decks to play: '])
            ->add('get', SubmitType::class, ['label' => 'Deal!'])
            ->getForm();

        $deckForm->handleRequest($request);

        if ($deckForm->isSubmitted() && $deckForm->isValid()) {
            $decks = $deckForm->getData()['decks'];
        } else {
            $decks = 1;
        }

        if ($cheatForm->isSubmitted() && $cheatForm->isValid()) {
            $hand = explode(',',$cheatForm->getData()['cards']);
            if (count($hand) != 5) {
                throw new \Exception('Invalid cards submission.');
            }
        } else {
            $dealer = $this->get('dealer');
            $hand = $dealer->mix($decks)->deal();
        }

        $evaluator = $this->get('evaluator');
        $result = $evaluator->getHighestCombination($hand, true);

        $highestCombination = $result[0];
        $combination = ["name" => $highestCombination[0], "cards" => $highestCombination[1]];
        $hand = $result[1];

        return $this->render('PokerhandBundle:Default:index.html.twig', [
            "decks" => $decks,
            "hand" => $hand,
            "combination" => $combination,
            "deckForm" => $deckForm->createView(),
            "cheatForm" => $cheatForm->createView()
        ]);
    }
}
