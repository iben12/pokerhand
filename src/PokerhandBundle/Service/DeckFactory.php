<?php

namespace PokerhandBundle\Service;


class DeckFactory
{
    private $cards;

    private $ranks = ["2","3","4","5","6","7","8","9","10","J","Q","K","A"];
    private $suits = ["H","D","C","S"];

    /**
     * @return array
     */
    public function getDeck()
    {
        $this->cards = [];
        $this->generateCards();
        return $this->cards;
    }

    private function generateCards()
    {
        foreach ($this->suits as $suit) {
            $suitCards = $this->makeSuit($suit);
            $this->cards = array_merge($suitCards, $this->cards);
        }
    }

    private function makeSuit($suit)
    {
        return array_map(function($rank) use ($suit) {
            return $rank.".".$suit;
        }, $this->ranks);
    }
}