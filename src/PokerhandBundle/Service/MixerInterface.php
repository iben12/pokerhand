<?php

namespace PokerhandBundle\Service;


interface MixerInterface
{
    /**
     * @param array $cards
     * @return array
     */
    public function mix($cards);
}