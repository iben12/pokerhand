<?php

namespace PokerhandBundle\Service;


class Dealer
{
    public $cards;

    private $mixer;

    /**
     * Dealer constructor.
     * @param MixerInterface $mixer
     */
    public function __construct(MixerInterface $mixer)
    {
        $this->mixer = $mixer;
        $this->cards = [];
    }

    /**
     * @param int $decks
     * @return $this
     */
    public function mix($decks = 1)
    {
        $this->cards = $this->mixer->mix( $this->getCards($decks) );
        return $this;
    }

    /**
     * @return array
     */
    public function deal()
    {
        return array_slice($this->cards, 0, 5);
    }

    /**
     * @param $decks integer
     * @return array
     */
    private function getCards($decks)
    {
        $deckFactory = new DeckFactory();

        for ($i=0; $i < $decks; $i++) {
            $this->cards = array_merge($this->cards, $deckFactory->getDeck());
        }

        return $this->cards;
    }

}