<?php


namespace PokerhandBundle\Service;


class ParsedHand
{
    public $hand;
    public $ranks = [];
    public $suits = [];
    public $rankCount;
    public $suitCount;
    public $ordering = [];
    private $suitSymbols = [
        "C" => "&#x2663",
        "S" => "&#x2660",
        "H" => "&#x2665",
        "D" => "&#x2666"
    ];

    /**
     * ParsedHand constructor.
     * @param array $hand
     * @param bool $withSymbols
     */
    public function __construct($hand, $withSymbols)
    {
        $this->parse($hand, $withSymbols);
        return $this;
    }

    public function getHighestCard()
    {
        arsort($this->ordering);
        reset($this->ordering);
        $card = $this->hand[key($this->ordering)];

        return $card;
    }

    public function orderedAsc()
    {
        asort($this->ordering);
        $ordered = [];

        foreach ($this->ordering as $key => $order) {
            $card = $this->hand[$key];
            $ordered[] = $card;
        }

        if (in_array(12,$this->ordering) && in_array(0,$this->ordering)) {
            $ace = $ordered[4];
            array_pop($ordered);
            array_unshift($ordered,$ace);
        }
        return $ordered;
    }

    public function getByRank($ranks)
    {
        if (!is_array($ranks)) {
            $ranks = [$ranks];
        }
        $cards = [];
        foreach ($ranks as $rank) {
            foreach ($this->ranks[$rank] as $card) {
                array_push($cards, $card);
            }
        }
        return $cards;
    }

    public function rankCounter()
    {
        $count = [];
        foreach(array_keys($this->ranks) as $rank) {
            $count[$rank] = count($this->ranks[$rank]);
        }
        $this->rankCount = $count;
    }

    public function suitCounter()
    {
        $count = [];
        foreach(array_keys($this->suits) as $suit) {
            $count[$suit] = count($this->suits[$suit]);
        }
        $this->suitCount = $count;
    }

    private function parse($hand, $formatted)
    {
        $this->ranks = [];
        $this->suits = [];

        foreach ($hand as $card) {
            $split = explode(".", $card);
            if ($formatted) {
                $split[1] = $this->suitSymbols[$split[1]];
            }
            $this->hand[] = $split;
            $this->ordering[] = $this->getOrder($split[0]);
            if (array_key_exists($split[1], $this->suits)) {
                array_push($this->suits[$split[1]], $split);
            } else {
                $this->suits[$split[1]] = [$split];
            }
            if (array_key_exists($split[0], $this->ranks)) {
                array_push($this->ranks[$split[0]], $split);
            } else {
                $this->ranks[$split[0]] = [$split];
            }
        }

        $this->rankCounter();
        $this->suitCounter();
    }

    private function getOrder($rank)
    {
        $order = ["2" => 0, "3" => 1, "4" => 2, "5" => 3, "6" => 4, "7" => 5, "8" => 6, "9" => 7, "10" => 8, "J" => 9, "Q" => 10, "K" => 11, "A" => 12];
        return $order[$rank];
    }
}