<?php


namespace PokerhandBundle\Service;


class BasicMixer implements MixerInterface
{
    /**
     * @param array $cards
     * @return array
     */
    public function mix($cards)
    {
        shuffle($cards);
        return $cards;
    }
}