<?php


namespace PokerhandBundle\Service;


class HandEvaluator
{
    /**
     * @var ParsedHand
     **/
    private $parsedHand;

    private $straits;

    public function __construct()
    {
        $this->straitsGenerator();
    }

    /**
     * @param array $hand
     * @param bool $withSymbols
     * @return array
     */
    public function getHighestCombination($hand, $withSymbols = false)
    {
        $this->parsedHand = new ParsedHand($hand, $withSymbols);

        return [$this->evaluate(), $this->parsedHand->hand];
    }

    private function evaluate()
    {
        if ($this->isFlush() && $this->isStrait()) {
            if ($this->isRoyalFlush()) {
                return ['royal flush', $this->parsedHand->orderedAsc()];
            }
            return ['strait flush', $this->parsedHand->orderedAsc()];
        }
        if ($this->isFourOfAKind()) {
            $rank = array_keys($this->parsedHand->rankCount,4);
            $cards = $this->parsedHand->getByRank($rank);
            return ['four of a kind', $cards];
        }
        if ($this->isFull()) {
            $drillRank = array_keys($this->parsedHand->rankCount,3);
            $pairRank = array_keys($this->parsedHand->rankCount,2);
            $ranks = array_merge($drillRank, $pairRank);
            $cards = $this->parsedHand->getByRank($ranks);
            return ['full house', $cards];
        }
        if ($this->isFlush()) {
            return ['flush', $this->parsedHand->orderedAsc()];
        }
        if ($this->isStrait()) {
            return ['strait', $this->parsedHand->orderedAsc()];
        }
        if ($this->isThreeOfAKind()) {
            $cards = $this->getCardsByRankCount(3);
            return ['three of a kind', $cards];
        }

        if ($this->isTwoPairs()) {
            $pairs = $this->getCardsByRankCount(2);
            return ['two pairs', $pairs];
        }

        if ($this->isPair()) {
            $pair = $this->getCardsByRankCount(2);
            return ['one pair', $pair];
        }

        return ['high card', [$this->parsedHand->getHighestCard()]];
    }

    private function isRoyalFlush()
    {
        if ( array_key_exists("A", $this->parsedHand->ranks) && array_key_exists("K", $this->parsedHand->ranks)) {
            return true;
        }
        return false;
    }

    private function isFourOfAKind()
    {
        if (in_array(4, $this->parsedHand->rankCount)) {
            return true;
        }
        return false;
    }

    private function isFull()
    {
        if (in_array(3, $this->parsedHand->rankCount) && in_array(2, $this->parsedHand->rankCount)) {
            return true;
        }
        return false;
    }

    private function isFlush()
    {
        return in_array(5, $this->parsedHand->suitCount);
    }

    private function isThreeOfAKind()
    {
        return in_array(3, $this->parsedHand->rankCount);
    }

    private function isStrait()
    {
        $cardValues = array_values($this->parsedHand->ordering);
        asort($cardValues);
        $sortedValues = [];
        foreach ($cardValues as $card) {
            $sortedValues[] = $card;
        }

        if (in_array($sortedValues, $this->straits)) {
            return true;
        }
        return false;
    }

    private function isTwoPairs()
    {
        if ( count(array_keys($this->parsedHand->rankCount, 2)) == 2 ) {
            return true;
        }
        return false;
    }

    private function isPair()
    {
        if (in_array(2, $this->parsedHand->rankCount)) {
            return true;
        }
        return false;
    }

    private function getCardsByRankCount($count)
    {
        $pairRanks = array_keys($this->parsedHand->rankCount,$count);
        return $this->parsedHand->getByRank($pairRanks);
    }

    private function straitsGenerator()
    {
        $straits = [
            [0, 1, 2, 3, 12]
        ];
        for ($i = 0; $i < 9; $i++) {
            array_push($straits, range($i,$i+4));
        }
        $this->straits = $straits;
    }
}