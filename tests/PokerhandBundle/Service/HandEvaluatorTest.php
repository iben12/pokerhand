<?php


namespace Tests\PokerhandBundle\Service;


use PHPUnit\Framework\TestCase;
use PokerhandBundle\Service\HandEvaluator;

class HandEvaluatorTest extends TestCase
{
    /**
     * @var HandEvaluator
     */
    public $evaluator;

    public function setUp()
    {
        $this->evaluator = new HandEvaluator();
    }
    public function testCreation()
    {
        $this->assertInstanceOf(
            HandEvaluator::class,
            $this->evaluator
        );
    }

    public function testRoyalFlush()
    {
        $hand1 = ["Q.S", "J.S", "K.S", "A.S", "10.S"];
        $hand2 = ["2.S", "5.S", "A.S", "4.S", "3.S"];

        $combination1 = $this->evaluator->getHighestCombination($hand1, false);
        $combination2 = $this->evaluator->getHighestCombination($hand2, false);

        $cards1 = [["10","S"], ["J","S"], ["Q","S"], ["K","S"], ["A","S"]];
        $cards2 = [["A","S"], ["2","S"], ["3","S"], ["4","S"], ["5","S"]];

        $this->assertEquals(
            ['royal flush',$cards1],
            $combination1[0]
        );

        $this->assertEquals(
            ['strait flush',$cards2],
            $combination2[0]
        );
    }

    public function testStraitFlush()
    {
        $hand1 = ["9.S", "K.S", "10.S", "Q.S", "J.S"];
        $combination1 = $this->evaluator->getHighestCombination($hand1, false);
        $cards = [["9","S"], ["10","S"], ["J","S"], ["Q","S"], ["K","S"]];

        $this->assertEquals(
            ['strait flush',$cards],
            $combination1[0]
        );
    }

    public function testFourOfAKind()
    {
        $hand1 = ["9.C", "9.S", "K.S", "9.H", "9.D"];
        $combination1 = $this->evaluator->getHighestCombination($hand1,false);
        $cards = [["9","C"], ["9","S"], ["9","H"], ["9","D"]];

        $this->assertEquals(
            ['four of a kind', $cards],
            $combination1[0]
        );
    }

    public function testFullHouse()
    {
        $hand1 = ["9.C", "9.H", "K.D", "K.S", "9.S"];
        $combination1 = $this->evaluator->getHighestCombination($hand1, false);
        $cards = [["9","C"], ["9","H"], ["9","S"], ["K","D"], ["K","S"]];

        $this->assertEquals(
            ['full house',$cards],
            $combination1[0]
        );
    }

    public function testFlush()
    {
        $hand1 = ["5.S", "3.S", "9.S", "A.S", "K.S"];
        $combination1 = $this->evaluator->getHighestCombination($hand1,false);
        $cards = [["3","S"], ["5","S"], ["9","S"], ["K","S"], ["A","S"]];

        $this->assertEquals(
            ['flush',$cards],
            $combination1[0]
        );
    }

    public function testStrait()
    {
        $hand1 = ["3.H", "5.D", "4.H", "6.S", "2.S"];
        $combination1 = $this->evaluator->getHighestCombination($hand1,false);
        $strait1 = [["2","S"], ["3","H"], ["4","H"], ["5","D"], ["6","S"]];

        $hand2 = ["9.S", "J.H", "10.H", "Q.D", "K.S"];
        $combination2 = $this->evaluator->getHighestCombination($hand2,false);
        $strait2 = [["9","S"], ["10","H"], ["J","H"], ["Q","D"], ["K","S"]];

        $hand3 = ["A.S", "J.H", "10.H", "Q.D", "K.S"];
        $combination3 = $this->evaluator->getHighestCombination($hand3,false);
        $strait3 = [["10","H"], ["J","H"], ["Q","D"], ["K","S"], ["A","S"]];

        $hand4 = ["3.H", "5.D", "4.H", "A.S", "2.S"];
        $combination4 = $this->evaluator->getHighestCombination($hand4,false);
        $strait4 = [["A","S"], ["2","S"], ["3","H"], ["4","H"], ["5","D"]];

        $this->assertEquals(
            ['strait',$strait1],
            $combination1[0]
        );
        $this->assertEquals(
            ['strait',$strait2],
            $combination2[0]
        );
        $this->assertEquals(
            ['strait',$strait3],
            $combination3[0]
        );
        $this->assertEquals(
            ['strait',$strait4],
            $combination4[0]
        );
    }

    public function testThreeOfAKind()
    {
        $hand1 = ["9.S", "9.C", "9.H", "Q.D", "K.S"];

        $combination1 = $this->evaluator->getHighestCombination($hand1,false);

        $this->assertEquals(
            ['three of a kind',[["9","S"], ["9","C"], ["9","H"]]],
            $combination1[0]
        );
    }
    public function testTwoPairs()
    {
        $hand1 = ["9.S", "9.C", "J.H", "J.D", "K.S"];

        $combination1 = $this->evaluator->getHighestCombination($hand1,false);

        $this->assertEquals(
            ['two pairs',[["9","S"], ["9","C"], ["J","H"], ["J","D"]]],
            $combination1[0]
        );
    }

    public function testPair()
    {
        $hand1 = ["6.S", "4.C", "J.H", "J.D", "K.S"];

        $combination1 = $this->evaluator->getHighestCombination($hand1,false);

        $this->assertEquals(
            ['one pair', [["J","H"], ["J","D"]]],
            $combination1[0]
        );
    }

    public function testHighCard()
    {
        $hand1 = ["6.S", "4.C", "J.H", "Q.D", "K.S"];

        $combination1 = $this->evaluator->getHighestCombination($hand1,false);

        $this->assertEquals(
            ['high card',[["K","S"]]],
            $combination1[0]
        );
    }
}