<?php


namespace Tests\PokerhandBundle\Service;


use PHPUnit\Framework\TestCase;
use PokerhandBundle\Service\BasicMixer;
use PokerhandBundle\Service\Dealer;

class DealerTest extends TestCase
{
    /** @var Dealer **/
    public $dealer;

    public function setUp()
    {
        $this->dealer = new Dealer(new BasicMixer());
    }

    public function testDecks()
    {
        $dealer = $this->dealer->mix(2);

        $this->assertEquals(
            104,
            count($dealer->cards)
        );
    }

    public function testDeal()
    {
        $cards = $this->dealer->mix()->deal();

        $this->assertEquals(
            5,
            count($cards)
        );
    }
}