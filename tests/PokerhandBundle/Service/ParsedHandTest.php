<?php


namespace Tests\PokerhandBundle\Service;


use PHPUnit\Framework\TestCase;
use PokerhandBundle\Service\ParsedHand;

class ParsedHandTest extends TestCase
{
    public function testOrder()
    {
        $hand = ["A.H","3.C","J.C","5.S","Q.D"];
        $parsedHand = new ParsedHand($hand, false);
        $ordered = [["3","C"],["5","S"],["J","C"],["Q","D"],["A","H"]];

        $hand2 = ["A.H","3.C","2.C","5.S","Q.D"];
        $parsedHand2 = new ParsedHand($hand2, false);
        $ordered2 = [["A","H"],["2","C"],["3","C"],["5","S"],["Q","D"]];

        $this->assertEquals(
            $ordered,
            $parsedHand->orderedAsc()
        );

        $this->assertEquals(
            $ordered2,
            $parsedHand2->orderedAsc()
        );
    }

    public function testRanks()
    {
        $hand = ["A.H","A.C","J.C","5.S","Q.D"];
        $parsedHand = new ParsedHand($hand, false);
        $aces = [["A","H"],["A","C"]];
        $this->assertEquals(
            $aces,
            $parsedHand->getByRank("A")
        );
    }

    public function testRankCount()
    {
        $hand = ["A.H","A.C","J.C","5.S","Q.D"];
        $parsedHand = new ParsedHand($hand, false);

        $this->assertEquals(
            2,
            $parsedHand->rankCount["A"]
        );
    }

    public function testSuitCount()
    {
        $hand = ["A.H","A.C","J.C","5.S","Q.D"];
        $parsedHand = new ParsedHand($hand, false);

        $this->assertEquals(
            2,
            $parsedHand->suitCount["C"]
        );
    }
}